package org.colliber.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class SSMLambda implements RequestStreamHandler {
    private final JSONParser jsonParser = new JSONParser();
    private final AWSSimpleSystemsManagement ssmClient = AWSSimpleSystemsManagementClientBuilder.defaultClient();

    @SuppressWarnings("unchecked")
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        LambdaLogger logger = context.getLogger();

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            JSONObject requestJson = (JSONObject) jsonParser.parse(reader);

            if (requestJson.get("queryStringParameters") != null) {
                logger.log(requestJson.toJSONString());
                JSONObject qsp = (JSONObject) requestJson.get("queryStringParameters");
                if (qsp.get("name") != null) {
                    String name;
                    if (qsp.get("name") instanceof JSONArray) {
                        JSONArray array = (JSONArray) qsp.get("name");
                        name = (String) array.get(0);
                    } else
                        name = (String) qsp.get("name");

                    logger.log(String.format("fetching parameter '%s'\n", name));

                    long startTime = System.nanoTime();

                    Response response = fetchParameter(name);

                    response.setMetric(System.nanoTime() - startTime);

                    logger.log(String.format("parameter '%s' has value '%s'\n", name, response.getParameterValue()));
                    logger.log(String.format("fetch execution time: %d ms\n", response.getMetric() / 1000000));

                    sendResponse(200, "application/json", response.convertToJson().toJSONString(), outputStream);
                } else {
                    throw new InvalidRequestException("query parameter 'name' missing");
                }
            } else {
                throw new InvalidRequestException("query parameter 'name' missing");
            }
        } catch (ParameterNotFoundException e) {
            sendResponse(404, "text/plain", e.toString(), outputStream);
        } catch (ParseException | InvalidRequestException e) {
            sendResponse(400, "text/plain", e.toString(), outputStream);
        }
    }

    @SuppressWarnings("unchecked")
    private void sendResponse(int statusCode, String contentType, String body, OutputStream outputStream) throws IOException {
        JSONObject responseJson = new JSONObject();

        JSONObject headerJson = new JSONObject();
        headerJson.put("Content-Type", contentType);

        responseJson.put("headers", headerJson);
        responseJson.put("statusCode", statusCode);
        responseJson.put("body", body);

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private Response fetchParameter(String name) throws ParameterNotFoundException {
        GetParameterRequest parameterRequest = new GetParameterRequest();
        parameterRequest.setName(name);

        GetParameterResult parameterResult;
        try {
            parameterResult = ssmClient.getParameter(parameterRequest);
        } catch (Exception e) {
            throw new ParameterNotFoundException(String.format("unable to retrieve parameter '%s'", name));
        }

        return new Response(parameterResult.getParameter().getName(), parameterResult.getParameter().getValue());
    }

    static class Response {
        private String parameterName;
        private String parameterValue;
        private long metric;

        public Response(String parameterName, String parameterValue) {
            this.parameterName = parameterName;
            this.parameterValue = parameterValue;
        }

        public String getParameterName() {
            return parameterName;
        }

        public void setParameterName(String parameterName) {
            this.parameterName = parameterName;
        }

        public String getParameterValue() {
            return parameterValue;
        }

        public void setParameterValue(String parameterValue) {
            this.parameterValue = parameterValue;
        }

        public long getMetric() {
            return metric;
        }

        public void setMetric(long metric) {
            this.metric = metric;
        }

        @SuppressWarnings("unchecked")
        public JSONObject convertToJson() {
            JSONObject json = new JSONObject();
            json.put("parameterName", parameterName);
            json.put("parameterValue", parameterValue);
            json.put("metric", metric);
            return json;
        }
    }

    private class ParameterNotFoundException extends RuntimeException {
        public ParameterNotFoundException(String message) {
            super(message);
        }
    }

    private class InvalidRequestException extends RuntimeException {
        public InvalidRequestException(String message) {
            super(message);
        }
    }
}
